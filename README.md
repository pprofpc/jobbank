# README #

#Job Bank#

Un espacio para compartir tus capacidades

<hr />

La idea principal de éste proyecto social es disponer un espacio digital en donde se den a conocer trabajos, negocios y/o distintas capacidades personales de los padres, madreo o tutores, de la comunidad del colegio ITS Villada de la Provincia de Córdoba. De éste modo conectar (en estos momentos tan difíciles) los servicios ofrecidos por un papá con las necesidades de otro.

##Es nuestro##
El sitio está cincuscripto a los padres de la comunidad del ITS Villada. Aunque vincula a las familias del Colegio, éste proyecto desvincula de toda responsabilidad a ésta comunidad.

##Social##
Estamos pasando un momento difícil y único, por lo que se busca un modo de ayudar.

##Libre##
Tanto la publicación como la consulta de los servicios ofrecidos es totalmente libre de todo costo y cargo.